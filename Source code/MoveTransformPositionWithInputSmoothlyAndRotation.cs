﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Moves the object relative to it's forward or right vector (does not account for the objects rotation)
// Rotates with Q & E, Moves with WASD
public class MoveTransformPositionWithInputSmoothlyAndRotation : MonoBehaviour
{
    [SerializeField] private float _moveSpeed = 2f;
    [SerializeField] private float _turnSpeed = 30f;
    //public Animator anim;
    public GameObject player;

    void Update()
    {

        //anim = GetComponentInChildren<Animator>();
        //anim = GetComponent<Animator>();

        if (Input.anyKey)
        {
            if (Input.GetKey(KeyCode.A))
                transform.Rotate(Time.deltaTime * _turnSpeed * Vector3.down);
        
            if (Input.GetKey(KeyCode.E))
                transform.Rotate(Time.deltaTime * _turnSpeed * Vector3.up);
                
            
            if (Input.GetKey(KeyCode.D))
                transform.position += Time.deltaTime * _moveSpeed * transform.right;
            
            if (Input.GetKey(KeyCode.Q))
                transform.position += Time.deltaTime * _moveSpeed * -transform.right; // negative right gives us left


            if (Input.GetKey(KeyCode.Z))
                transform.position += Time.deltaTime * _moveSpeed * transform.forward;


            if (Input.GetKey(KeyCode.S))
                transform.position += Time.deltaTime * _moveSpeed * -transform.forward; // negative forward gives us backward


            player.GetComponent<Animator>().Play("Orc_wolfrider_03_run");
            
        }

        else
        {
            player.GetComponent<Animator>().Play("Orc_wolfrider_05_combat_idle");
        }

    }

}
