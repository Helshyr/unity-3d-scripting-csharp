using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class WolfManager : MonoBehaviour
{

    public Transform WolfPrefab1;
    public Transform WolfPrefab2;
    public Transform WolfPrefab3;
    public Transform spawn;
    public int nbWolfCreate = 8;

    private static WolfManager instance = null;
    public static WolfManager sharedInstance
    {
        get
        {
        if (instance == null)
        {
            instance = GameObject.FindObjectOfType<WolfManager>();
        }
        return instance;
        }
    }
 
    //public Transform prefabWolf;
    private List<Transform> wolfs = new List<Transform>();

    // Start is called before the first frame update
    void Start()
    {
        Transform wolf = GameObject.Instantiate<Transform>(WolfPrefab1);
        for(int i = 0; i < nbWolfCreate; i++)
        {
            //Creation des loups, en faisant des modeles de maniere aleatoire

            int r = Random.Range(1,3);

            if (r==1)
            {
                wolf = GameObject.Instantiate<Transform>(WolfPrefab1);
            }

            if (r==2)
            {
                wolf = GameObject.Instantiate<Transform>(WolfPrefab2);
            }

            if (r==3)
            {
                wolf = GameObject.Instantiate<Transform>(WolfPrefab3);
            }
            
            wolf.parent = transform; 

            float spawnX = spawn.position.x;
            float spawnY = spawn.position.y;
            float spawnZ = spawn.position.z;

            // je les separe
            wolf.position = new Vector3(spawnX + Random.Range(-20f, 20f), spawnY, spawnZ + Random.Range(-20f, 20f));
            wolfs.Add(wolf);
        } 
    }

    public ReadOnlyCollection<Transform> readWolfs
    {
        // pour qu'ils soient consultables
        get {return new ReadOnlyCollection<Transform>(wolfs);}
    }

}
