using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfPack : MonoBehaviour
{
    // Beaucoup de public pour debugger dans l'editeur
    public GameObject wolf;
    public Transform joueur;
    public Transform dragon;
    public float vitesse = 9f;
    public float distanceLimiteProcheJoueur = 30;
    public float distanceLimiteLoinDragon = 150;
    public float distanceLimiteLoinMate = 8;
    public float distanceProtection = 20;
    private bool atLimitePoursuivre = false;
    public Transform spawn;
    public float distancetropProche = 5;

    public float distanceLoupJoueur = 0;
    public float distanceLoupDragon = 0;

/////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////FUNCTIONS/////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////


    private bool atDestination(float distance, float distanceStop)
    {
        float distanceRestante = distance - distanceStop;
        return distanceRestante <= 0;
    }

/////////


    private void reachMate()
    {
        // fonction pour rejoindre le plus proche loup
        float plusCourt = 99999999999999;
        Vector3 end = new Vector3(0,0,0);
        Vector3 beginning = new Vector3(0,0,0);
        Vector3 deplacementPlusCourt = new Vector3(0,0,0);

        foreach (Transform otherWolf in WolfManager.sharedInstance.readWolfs)
        {
            Vector3 deplacementAutreLoup = otherWolf.position - transform.position;
            float distanceAutreLoup = Vector3.Distance(otherWolf.position, transform.position);

            if (distanceAutreLoup < plusCourt)
            {
                deplacementPlusCourt = deplacementAutreLoup;
                end = otherWolf.position;
                beginning = transform.position;
            }

        
            deplacementPlusCourt = deplacementPlusCourt.normalized * vitesse * Time.deltaTime;
        }
        
        if (!atDestination(Vector3.Distance(end, beginning), Random.Range(5f, 15f)))
        {
            transform.position += deplacementPlusCourt;
            
            if (deplacementPlusCourt != Vector3.zero)
            {
                transform.forward = deplacementPlusCourt;
            }  

        }

        


    }

/////////

    private bool isDragonProtected()
    {
        // vérifie si le dragon a des gardes
        float compteur = 0;
        foreach (Transform otherWolf in WolfManager.sharedInstance.readWolfs)
        {
            float distanceLoupDragon = Vector3.Distance(otherWolf.position, dragon.position);

            if (distanceLoupDragon < distanceProtection)
            {
                compteur += 1;
            }

        }

        return compteur >=3;

    }

/////////

    private bool doIProtectDragon()
    {
        return Vector3.Distance(dragon.position, transform.position) <= distanceProtection;
    }


//////////

    private int nbPLayerPoursuivit()
    {
        int compteur = 0;
        foreach (Transform otherWolf in WolfManager.sharedInstance.readWolfs)
        {
            float distance = Vector3.Distance(otherWolf.position, joueur.position);

            if (distance <= distanceLimiteProcheJoueur)
            {
                compteur += 1;
            }

        }

        return compteur;    
    }



//////////


    private void spread()
        {
            // se repartir de manière plus naturelle
            float spawnX = spawn.position.x;
            float spawnY = spawn.position.y;
            float spawnZ = spawn.position.z;

            Vector3 newPosition = new Vector3(spawnX + Random.Range(-50f, 50f), spawnY, spawnZ + Random.Range(-50f, 50f));
            Vector3 deplacement = joueur.position - transform.position;
            deplacement = deplacement.normalized * vitesse * Time.deltaTime;
            float distance = Vector3.Distance(newPosition, transform.position);
            if(!atDestination(distance, 1))
            {
                transform.position += deplacement;
                
                if (deplacement != Vector3.zero)
                {
                    transform.forward = deplacement;
                }        

            }

        }

//////////

    private bool isLoinDragon()
    {

        float distance = Vector3.Distance(dragon.position, transform.position);
        return distance > distanceLimiteLoinDragon;    
    }

///////////

    private void repulse()
    {
        // s'eloigner
        foreach (Transform otherWolf in WolfManager.sharedInstance.readWolfs)
        {
            float distance = Vector3.Distance(otherWolf.position, transform.position);
            Vector3 deplacement = otherWolf.position - transform.position;
            deplacement = deplacement.normalized * vitesse * Time.deltaTime;

            if(distance <= distancetropProche)
            {
                if(!atDestination(distance, 1))
                {
                    transform.position -= deplacement;
                    
                    if (deplacement != Vector3.zero)
                    {
                        transform.forward = deplacement;
                    }        

                }

            }
        }
    }


////////

    private void repulse2()
    {
        // deuxieme essai pour la fonction s'eloigner
        foreach (Transform otherWolf in WolfManager.sharedInstance.readWolfs)
        {
            float distance = Vector3.Distance(otherWolf.position, transform.position);
            Vector3 deplacement = otherWolf.position - transform.position;
            deplacement = deplacement.normalized * vitesse * Time.deltaTime;

            if(distance <= distancetropProche)
            {


                Vector3 newP = new Vector3(transform.position.x - 1, transform.position.y, transform.position.z - 1);
                Vector3 newd = newP - transform.position;
                newd = newd.normalized * vitesse * Time.deltaTime;

                if(!atDestination(distance, 0.1f))
                {
                    transform.position += newd;
                    
                    if (deplacement != Vector3.zero)
                    {
                        transform.forward = newd;
                    }        

                }

            }
        }
    }

/////////

    private bool isToFarDragon()
    {
        float distance = Vector3.Distance(dragon.position, transform.position);

        return distance > distanceLimiteLoinDragon;
    }


/////////

    private bool isToFarMate()
    {
        float plusCourt = 99999999999999;
        Vector3 end = new Vector3(0,0,0);
        Vector3 beginning = new Vector3(0,0,0);
        Vector3 deplacementPlusCourt = new Vector3(0,0,0);

        foreach (Transform otherWolf in WolfManager.sharedInstance.readWolfs)
        {
            Vector3 deplacement = otherWolf.position - transform.position;
            float distance = Vector3.Distance(otherWolf.position, transform.position);

            if (distance < plusCourt)
            {
                deplacementPlusCourt = deplacement;
                end = otherWolf.position;
                beginning = transform.position;
            }

        
            deplacementPlusCourt = deplacementPlusCourt.normalized * vitesse * Time.deltaTime;
        }
        

        return Vector3.Distance(end, beginning) > distanceLimiteLoinMate;
    }

////////////

    private bool isToCloseMate()
    {
        float plusCourt = 99999999999999;
        Vector3 end = new Vector3(0,0,0);
        Vector3 beginning = new Vector3(0,0,0);
        Vector3 deplacementPlusCourt = new Vector3(0,0,0);

        foreach (Transform otherWolf in WolfManager.sharedInstance.readWolfs)
        {
            Vector3 deplacement = otherWolf.position - transform.position;
            float distance = Vector3.Distance(otherWolf.position, transform.position);

            if (distance < plusCourt)
            {
                deplacementPlusCourt = deplacement;
                end = otherWolf.position;
                beginning = transform.position;
            }

        
            deplacementPlusCourt = deplacementPlusCourt.normalized * vitesse * Time.deltaTime;
        }
        

        return Vector3.Distance(end, beginning) < distancetropProche;
    }


////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////MAIN/////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////


    // Update is called once per frame
    void Update()
    {
        /* loup et joueur */

        // calcul du deplacement a effectuer
        Vector3 deplacementLoupJoueur = joueur.position - transform.position;
        deplacementLoupJoueur = deplacementLoupJoueur.normalized * vitesse * Time.deltaTime;
        // verifier si le joueur cible n'est pas trop proche (loup)
        distanceLoupJoueur = Vector3.Distance(joueur.position, transform.position);
        atLimitePoursuivre = distanceLoupJoueur <= distanceLimiteProcheJoueur;

        /* loup et dragon */

        Vector3 deplacementLoupDragon = dragon.position - transform.position;
        deplacementLoupDragon = deplacementLoupDragon.normalized * vitesse * Time.deltaTime;
        // verifier si loup pas trop loin dragon
        distanceLoupDragon = Vector3.Distance(dragon.position, transform.position);

        var p1 = transform.position;

// test 1. le code ci dessous peut etre decommente pour le tester, il s'agit d'une version differente du code test 2

/*
        if (atLimitePoursuivre)
        {
            if (!isToFarDragon())
            {
                if (isToCloseMate())
                {
                    repulse2();
                }

                else
                {


                    if(!atDestination(distanceLoupJoueur, 1))
                    {
                        transform.position += deplacementLoupJoueur;
                        
                        if (deplacementLoupJoueur != Vector3.zero)
                        {
                            transform.forward = deplacementLoupJoueur;
                        }        

                    }
                         
                          
                }

            }

            else
            {
                if(!atDestination(distanceLoupDragon, 10))
                {
                    transform.position += deplacementLoupDragon;
                    
                    if (deplacementLoupDragon != Vector3.zero)
                    {
                        transform.forward = deplacementLoupDragon;
                    }        

                }

            }

        }

        else
        {
            if(doIProtectDragon())
            {
                if(isToFarMate())
                {
                    reachMate();
                }

                if(isToCloseMate())
                {
                    repulse2();
                }
            }

            else
            {
                if(isDragonProtected())
                {
                    if(isToFarMate())
                    {
                        while(!isToCloseMate())
                        {
                            reachMate();
                        }
                    } 

                    if (isToCloseMate())
                    {
                        repulse2();
                    }

                }

                else
                {
                    if(!atDestination(distanceLoupDragon, 10))
                    {
                        transform.position += deplacementLoupDragon;
                        
                        if (deplacementLoupDragon != Vector3.zero)
                        {
                            transform.forward = deplacementLoupDragon;
                        }        

                    }

                }
            }
        }

    }
}

*/

// debut du code test 2

        if(atLimitePoursuivre)
        {

            if (isToFarDragon())//(!isDragonProtected())
            {
                if(!atDestination(distanceLoupDragon, 15))
                {
                    transform.position += deplacementLoupDragon;
                    
                    if (deplacementLoupDragon != Vector3.zero)
                    {
                        transform.forward = deplacementLoupDragon;
                    }        

                }

            }

            else
            {

                if(!atDestination(distanceLoupJoueur, 5))
                {
                    transform.position += deplacementLoupJoueur;
                    
                    if (deplacementLoupJoueur != Vector3.zero)
                    {
                        transform.forward = deplacementLoupJoueur;
                    }        

                }
 
            }
        }

        else
        {

            // checker le nbr de gens qui protege dragon et check nbr suivant joueur -> retour au dragon
            // repartition de la meute qui est au dragon

            if (!isDragonProtected())
            {

                if(!atDestination(distanceLoupDragon, 10))
                {
                    transform.position += deplacementLoupDragon;
                    
                    if (deplacementLoupDragon != Vector3.zero)
                    {
                        transform.forward = deplacementLoupDragon;
                    }        

                }
            }
            
            else
            {

                reachMate();
            
                
            }

         
        }
        

    var p2 = transform.position;

    if(p1 != p2)
    {
        wolf.GetComponent<Animator>().Play("run");
    }
    else
    {
        wolf.GetComponent<Animator>().Play("breathes");
    }

        
        
    }
}
