using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dragon : MonoBehaviour
{
    public GameObject dragonObj;
    public Transform theDragon;
    public Transform player;

    public float distanceAttaque = 30f;

    // Update is called once per frame

    void Update()
    {
        // script uniquement pur l'animation
        float distance = Vector3.Distance(theDragon.position, player.position);
        if (distance <= distanceAttaque)
        {

            dragonObj.GetComponent<Animator>().Play("Claw Attack");

        }
        else
        {

            dragonObj.GetComponent<Animator>().Play("Scream");        
        }

    }
}
